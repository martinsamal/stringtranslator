package cz.starkys.stringTranslator;

import cz.starkys.stringTranslator.service.PigLatinParser;
import cz.starkys.stringTranslator.service.impl.PigLatinParserImpl;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * @author Martin Samal (martin.samal@inventi.cz) on 08.07.2019.
 * Test suite for {@link PigLatinParser}
 */

public class WordTranslationTestSuite {

    private PigLatinParser parser = new PigLatinParserImpl();

    @Test(dataProvider = "testPhrasesDataProvider")
    public void phrasesTest(String expected, String expectedResult) {
        String mcCloudTest = parser.parse(expected);
        Assert.assertEquals(mcCloudTest, expectedResult, "Output was not parsed as expected.");
    }

    @DataProvider(name = "testPhrasesDataProvider")
    public  Object[][] testPhrasesDataProvider() {
        return new Object[][] {
                {"Hello", "Ellohay"},
                {"Hello.", "Ellohay."},
                {"apple", "appleway"},
                {"can`t", "antca`y"},
                {"can`t.", "antca`y."},
                {"this-thing", "histay-hingtay"},
                {"Beach", "Eachbay"},
                {"McCloud", "CcLoudmay"},
                {"end.", "endway."},
                {"End.", "Endway."},
                {"this-thing. Beach can`t.", "histay-hingtay. Eachbay antca`y."}
        };
    }
}
