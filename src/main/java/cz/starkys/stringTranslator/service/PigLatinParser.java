package cz.starkys.stringTranslator.service;

/**
 * @author Martin Samal (martin.samal@inventi.cz) on 07.07.2019.
 * Service for translating string into “pig-latin” using defined rules
 */
public interface PigLatinParser {

    String parse(String inputString);
}
