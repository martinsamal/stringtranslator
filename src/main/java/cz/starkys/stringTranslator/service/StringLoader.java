package cz.starkys.stringTranslator.service;

/**
 * @author Martin Samal (martin.samal@inventi.cz) on 06.07.2019.
 * String data loader
 */
public interface StringLoader {

    /**
     * Load resource file as String
     * @return text from resource, null if resource is not found
     */
    String loadFromResource();
}
