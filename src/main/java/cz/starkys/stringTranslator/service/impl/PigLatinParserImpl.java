package cz.starkys.stringTranslator.service.impl;

import cz.starkys.stringTranslator.service.PigLatinParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Martin Samal (martin.samal@inventi.cz) on 07.07.2019.
 */
public class PigLatinParserImpl implements PigLatinParser {

    private static final String WORD_SPLIT_PRESERVING_SPLITTER_REGEX = "(?<=[\\s\\,\\.\\?\\!\\-])";
    private static final String WORD_SPLIT_REGEX = "[\\s\\,\\.\\?\\!\\-]";
    private static final String PUNCTUATION_REGEX = "(?=\\p{Punct})";
    private static final String[] VOWELS = new String[] {"a", "e", "i", "y", "o", "u"};
    private static final String SKIPPED_SUFFIX = "way";
    private static final String CONSONANT_WORD_APPEND_SUFFIX = "ay";

    @Override
    public String parse(String inputString) {
        /** split string with given regex */
        List<String> words = Arrays.stream(inputString.split(WORD_SPLIT_PRESERVING_SPLITTER_REGEX)).collect(Collectors.toList());
        List<String> modifiedWords = words.stream().map( singleWord -> {
            /** skip words with {@link PigLatinParserImpl#SKIPPED_SUFFIX} on the end */
            if (singleWord.endsWith(SKIPPED_SUFFIX)) {
                return singleWord;
            } else {
                String inputWordWithoutLastChar;
                String lastCharGlue = singleWord.substring(singleWord.length()-1);
                if (lastCharGlue.matches(WORD_SPLIT_REGEX)) {
                    /** first, remove trailing char, since it is a punctuation or a space and will be used later as a glue */
                    inputWordWithoutLastChar = singleWord.substring(0, singleWord.length()-1);
                } else {
                    inputWordWithoutLastChar = singleWord;
                    lastCharGlue = "";
                }

                StringBuilder stringBuilder = new StringBuilder(inputWordWithoutLastChar.toLowerCase());
                /** case of two or more spaces, special chars etc. */
                if (inputWordWithoutLastChar.length() == 0) {
                    return stringBuilder.append(lastCharGlue).toString();
                }

                if (startsWithVowel(inputWordWithoutLastChar)) {
                    /** vowel words */
                    parseVowelLogic(stringBuilder);
                } else {
                    /** consonant words */
                    parseConsonantLogic(stringBuilder, inputWordWithoutLastChar);
                }

                correctPunctuation(stringBuilder, inputWordWithoutLastChar);
                correctCapitalization(stringBuilder, inputWordWithoutLastChar);

                /** append subtracted trailing char back */
                return stringBuilder.append(lastCharGlue).toString();
            }
        }).collect(Collectors.toList());

        /** collect string back */
        StringBuilder stringBuilder = new StringBuilder();
        modifiedWords.forEach(s -> stringBuilder.append(s));
        String result = stringBuilder.toString();

        return result;
    }

    // HELPER METHODS

    /**
     * finds vowel
     * @param word - examined word
     * @return true if vowel is found, false if it is a consonant
     */
    private boolean startsWithVowel(String word) {
        String firsLoweredLetter = word.substring(0, 1).toLowerCase();
        Optional<String> vowelFound = Arrays.stream(VOWELS).filter(vowel -> vowel.contains(firsLoweredLetter)).findAny();

        return vowelFound.isPresent();
    }

    /**
     * Words that start with a vowel have the letters “way” added to the end
     * @param stringBuilder - string builder with modified string
     */
    private void parseVowelLogic(StringBuilder stringBuilder) {
        stringBuilder.append("way");
    }

    /**
     * Move punctuation inside builder by the rule: "Punctuation must remain in the same relative place from the end of the word."
     * @param stringBuilder - string builder with modified string
     * @param inputWord - original input word to be modified
     */
    private void correctPunctuation(StringBuilder stringBuilder, String inputWord) {
        String[] punctuationSplit = inputWord.split(PUNCTUATION_REGEX);
        if (punctuationSplit.length > 1) {
            String punctuation = punctuationSplit[1].substring(0, 1);
            stringBuilder.deleteCharAt(stringBuilder.indexOf(punctuation));
            stringBuilder.insert(stringBuilder.toString().length()-(punctuationSplit[1].length()-1), punctuation);
        }
    }

    /**
     * Words that start with a consonant have their first letter moved to the end of the word and the letters “ay” added to the end.
     * @param stringBuilder - string builder with modified string
     * @param inputWord - original input word to be modified
     */
    private void parseConsonantLogic(StringBuilder stringBuilder, String inputWord) {
        String firstLetter = inputWord.substring(0, 1);
        stringBuilder.deleteCharAt(0);
        stringBuilder.append(firstLetter.toLowerCase()).append(CONSONANT_WORD_APPEND_SUFFIX);
    }

    /**
     * Preserves capitalization on the same place as in original word
     * @param inputWord - original input word to be modified
     * @param stringBuilder - string builder with modified string
     */
    private void correctCapitalization(StringBuilder stringBuilder, String inputWord) {
        List<Integer> upperCaseIndices = new ArrayList<>();
        for (int i=0; i<inputWord.length(); i++) {
            if (Character.isUpperCase(inputWord.charAt(i))) {
                upperCaseIndices.add(i);
            }
        }
        upperCaseIndices.stream().forEach(uppercaseIndex -> stringBuilder.replace(uppercaseIndex, uppercaseIndex+1, stringBuilder.substring(uppercaseIndex, uppercaseIndex+1).toUpperCase()));
    }
}
