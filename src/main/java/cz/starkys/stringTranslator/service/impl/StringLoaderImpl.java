package cz.starkys.stringTranslator.service.impl;

import cz.starkys.stringTranslator.service.StringLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 * @author Martin Samal (martin.samal@inventi.cz) on 06.07.2019.
 */
public class StringLoaderImpl implements StringLoader {

    private static final Logger logger = LogManager.getLogger();

    public static final String INPUT_TEXT_RESOURCE = "inputText.txt";

    @Override
    public String loadFromResource() {
        String resourceAsString = null;
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(INPUT_TEXT_RESOURCE);
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            resourceAsString = bufferedReader.lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (Exception e) {
            logger.error("Source file '{}' was not found", INPUT_TEXT_RESOURCE);
        }

        return resourceAsString;
    }
}
