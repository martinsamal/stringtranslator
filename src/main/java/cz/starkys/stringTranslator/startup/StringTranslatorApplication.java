package cz.starkys.stringTranslator.startup;

import cz.starkys.stringTranslator.service.PigLatinParser;
import cz.starkys.stringTranslator.service.StringLoader;
import cz.starkys.stringTranslator.service.impl.PigLatinParserImpl;
import cz.starkys.stringTranslator.service.impl.StringLoaderImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Martin Samal (martin.samal@inventi.cz) on 05.07.2019.
 * Entrypoint for spplication
 */

public class StringTranslatorApplication {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        logger.info("App started...");
        StringLoader stringLoader = new StringLoaderImpl();
        String inputData = stringLoader.loadFromResource();

        PigLatinParser pigLatinParser = new PigLatinParserImpl();
        String result = pigLatinParser.parse(inputData);

        logger.info(result);
    }
}
